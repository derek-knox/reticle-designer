import { action, observable } from 'mobx';
import domtoimage from "../../vendor-forks/dom-to-image";
import FileSaver from "file-saver";

import { getEditAreaInfo } from '../../utils/reticleUtils';

import { ReticleModel } from "../models/ReticleModel";

export class EditReticleStore {

    @observable isDrawing = false;
    @observable isSnapshotInProcess = false;
    @observable isGridControlOpen = false;
    @observable isMinimized = false;

    @observable positionHelperReticleInFocus = null;

    @observable graphics = [];
    @observable editAreaInfo = null;

    @observable snapshotScale = 1;

    constructor(stores) {
        this.stores = stores;
    }

    @action updatePositionHelperReticleInFocus(payload) {
        if (payload) {
            const newPayload = Object.assign({ dummyId: -1 }, payload);
            this.positionHelperReticleInFocus = new ReticleModel(newPayload);
            this.positionHelperReticleInFocus.color.settings.val = this.stores.colorStore.getHelperColor();
        } else {
            this.positionHelperReticleInFocus = null;
        }
    }

    @action updateGraphics(payload) {
        this.graphics = payload;
    }

    @action updateEditArea(payload) {
        this.editAreaInfo = getEditAreaInfo(payload);
    }

    @action updateSnapshotScale() {
        this.snapshotScale = this.snapshotScale === 5 ? 1 : this.snapshotScale + 1;
    }

    @action takeSnapshot() {
        this.isSnapshotInProcess = true;
        const target = document.getElementById("reticles-snapshot-target");
        const targetRect = target.getBoundingClientRect();
        domtoimage.toBlob(target, {
            quality: 1,
            bgcolor: 'transparent',
            width: targetRect.width * this.snapshotScale,
            height: targetRect.height * this.snapshotScale,
            style: {
                'transform': 'scale(' + this.snapshotScale + ')',
                'transform-origin': 'top left'
            }
        }).then(this.onShapshotProcessed);
    }

    @action.bound onShapshotProcessed(payload) {
        FileSaver.saveAs(payload, "fui-reticles-" + Date.now() + ".png");
        this.isSnapshotInProcess = false;
    }

}